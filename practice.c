#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MASTER	0

int main (int argc, char* argv[]) {
	int rank, size, startIndex, finalIndex, i;

	int arraySize = 60000;	
	double tstart,tfill, tsort, tmsort, tfglobals, tfglobale, tsglobals, tsglobale;
	FILE *fptr;

	MPI_Init (&argc, &argv);
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);
	MPI_Comm_size (MPI_COMM_WORLD, &size);

	int parallelArraySize = arraySize / (size-1);

	tstart = MPI_Wtime ();
	if (rank == MASTER) {
	   	printf("MASTER: Number of MPI tasks is: %d\n", size);
	} else {

		int partialArray[parallelArraySize];
	
		startIndex = 0;
		finalIndex = parallelArraySize;
		
		for(i = startIndex; i <= finalIndex; i++) {
			partialArray[i] = rand()%20;
		}

		// in Datei ausgeben
		// char fileName[100];
		// sprintf(fileName, "%s%d%s", "array_", rank, ".txt");
		// fptr = fopen(fileName, "w");
		// 	fprintf(fptr, "==== %d ====\n", rank);
		// for(i = startIndex; i <= finalIndex; i++) {
		// 	fprintf(fptr,"[%d] => %d\n",i, partialArray[i]);
		// }	
  //  		fclose(fptr); 

   		tfill = (MPI_Wtime () - tstart);
		printf("[%d]parallel fill: %f s \n", rank, tfill);

		//Bubblesort
		int i, j, temp;
     	for (i = startIndex; i <= (finalIndex - 1); ++i)
     	{
          for (j = startIndex; j <= finalIndex - 1; ++j ) {
               if (partialArray[j] > partialArray[j+1])
               {
                    temp = partialArray[j+1];
                    partialArray[j+1] = partialArray[j];
                    partialArray[j] = temp;
               }
          	}
     	}
     	// in Datei ausgeben
		// sprintf(fileName, "%s%d%s", "array_", rank, "_sorted.txt");
		// fptr = fopen(fileName, "w");
		// 	fprintf(fptr, "==== %d ====\n", rank);
		// for(i = startIndex; i <= finalIndex; i++) {
		// 	fprintf(fptr,"[%d] => %d\n",i, partialArray[i]);
		// }	
  //  		fclose(fptr);

   		tsort = (MPI_Wtime () - tstart)-tfill;
		printf("[%d]parallel sort: %f s \n",rank, tsort);

		//printf("Task: %d ; startIndex: %d ; finalIndex: %d \n", rank, startIndex, finalIndex);

	    MPI_Send(&partialArray, parallelArraySize, MPI_INT, MASTER, 0, MPI_COMM_WORLD);

	}
	

	if(rank == MASTER) {
		int i, r, j, k;
		int largeArray[arraySize];
		int partialArray[parallelArraySize];
		for (i = 1; i < size; i++) {
		    MPI_Recv(&partialArray, parallelArraySize, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		    //printf("[MASTER] received data from process %d\n", i);
			if(i != 1) {
				startIndex = ((arraySize / (size-1)) * (i-1));
			} else {
				startIndex = 0;
			}

			for(j = 0; j < parallelArraySize; j++) {
				largeArray[startIndex+j] = partialArray[j];
			}
		}

   		tsglobals = (MPI_Wtime () - tstart) ;

   		// Insertation Sort
   		int c, d, t;
		 
		for (c = 1 ; c <= arraySize - 1; c++) {
			d = c;
		 
			while ( d > 0 && largeArray[d] < largeArray[d-1]) {
		    	t          = largeArray[d];
		    	largeArray[d]   = largeArray[d-1];
		    	largeArray[d-1] = t;
		 
		    	d--;
	  		}
		}

		// char fileName[100];
		// sprintf(fileName, "%s", "final.txt");
		// fptr = fopen(fileName, "w");
		// for(i = 0; i < arraySize; i++) {
		// 	fprintf(fptr,"[%d] => %d\n",i, largeArray[i]);
		// }	
  //  		fclose(fptr);

   		tsglobale = (MPI_Wtime () - tstart)-tsglobals ;
		printf("[MASTER] final sort: %f s \n", tsglobale);
		printf("[MASTER] script time: %f s \n", (MPI_Wtime () - tstart));
	}
	MPI_Finalize();
	return 0;
}
